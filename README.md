## Добавьте переменную окружения:

```sh
export TOKEN=какие-то данные!!!
```

### Если вы хотите запустить ручками, то используйте вот эти команды:

```sh
cd tg_bot_cpp
docker build -t bot:1 . 
docker run --rm -d -it -e TOKEN=$TOKEN -v $(pwd)/data.csv:/bot/data.csv bot:1
```

### Если вы хотите использовать docker-compose, то введите лсдкющую команду:

```sh
docker-compose up -d
```
но для этого вам сначала придёться написать файлик docker-compose.yaml (=
