#include <tgbot/tgbot.h>

#include <algorithm>
#include <csignal>
#include <cstdio>
#include <cstdlib>
#include <exception>
#include <string>
#include <vector>

#include "AccForMoney.hpp"

#define MSG_LAYOUT "/profit категории доходов\n/wastage категории расходов"

using namespace std;
using namespace TgBot;

AccForMoney acc(PATH);
bool wait_input = false;
string categ;

void create_keyboard(const vector<string>& buttonStrings,
                     ReplyKeyboardMarkup::Ptr& kb);
void create_keyboard(const vector<vector<string>>& buttonLayout,
                     ReplyKeyboardMarkup::Ptr& kb);
bool is_number(const string& s) {
  printf("s: %s\n", s.data());
  return !s.empty() && std::find_if(s.begin(), s.end(), [](unsigned char c) {
                         return !std::isdigit(c);
                       }) == s.end();
}

int main() {
  string token(getenv("TOKEN"));
  printf("Token: %s\n", token.c_str());

  vector<BotCommand::Ptr> commands;
  BotCommand::Ptr cmdArray(new BotCommand);
  cmdArray->command = "profit";
  cmdArray->description = "+++";
  commands.push_back(cmdArray);

  cmdArray = BotCommand::Ptr(new BotCommand);
  cmdArray->command = "wastage";
  cmdArray->description = "---";
  commands.push_back(cmdArray);

  cmdArray = BotCommand::Ptr(new BotCommand);
  cmdArray->command = "show";
  cmdArray->description = "look data at now";
  commands.push_back(cmdArray);

  cmdArray = BotCommand::Ptr(new BotCommand);
  cmdArray->command = "save";
  cmdArray->description = "write data";
  commands.push_back(cmdArray);

  InlineKeyboardButton::Ptr checkButton(new InlineKeyboardButton);
  checkButton->text = "check";
  checkButton->callbackData = "check";

  Bot bot(token);

  bot.getApi().setMyCommands(commands);

  ReplyKeyboardMarkup::Ptr keyboard_profit(new ReplyKeyboardMarkup);
  create_keyboard(profit_categories, keyboard_profit);

  ReplyKeyboardMarkup::Ptr keyboard_wastage(new ReplyKeyboardMarkup);
  create_keyboard(wastage_categories, keyboard_wastage);

  bot.getEvents().onCommand(
      "start", [&bot, &keyboard_profit](Message::Ptr message) {
        bot.getApi().sendMessage(message->chat->id, "Hello! (=", false, 0,
                                 keyboard_profit);
      });
  bot.getEvents().onCommand(
      "profit", [&bot, &keyboard_profit](Message::Ptr message) {
        bot.getApi().sendMessage(message->chat->id, MSG_LAYOUT, false, 0,
                                 keyboard_profit);
      });
  bot.getEvents().onCommand(
      "wastage", [&bot, &keyboard_wastage](Message::Ptr message) {
        bot.getApi().sendMessage(message->chat->id, MSG_LAYOUT, false, 0,
                                 keyboard_wastage);
      });
  bot.getEvents().onCommand(
      "show", [&bot, &keyboard_wastage](Message::Ptr message) {
        bot.getApi().sendMessage(message->chat->id, acc.str());
      });
  bot.getEvents().onCommand(
      "save", [&bot, &keyboard_wastage](Message::Ptr message) {
        acc.save();
        bot.getApi().sendMessage(message->chat->id, "Data is wrote.");
      });

  bot.getEvents().onAnyMessage([&](Message::Ptr message) {
    printf("User wrote %s\n", message->text.c_str());
    if (StringTools::startsWith(message->text, "/start") ||
        StringTools::startsWith(message->text, "/profit") ||
        StringTools::startsWith(message->text, "/wastage") ||
        StringTools::startsWith(message->text, "/show") ||
        StringTools::startsWith(message->text, "/save")) {
      return;
    }

    if (acc.in(message->text.c_str())) {
      bot.getApi().sendMessage(message->chat->id, "Введите сумму:");
    } else if (acc.wait_input && is_number(message->text.c_str())) {
      printf("amount: %d\n", atoi(message->text.c_str()));
      acc.add(atoi(message->text.c_str()));
      stringstream ss;
      ss << acc.last_query << " += " << message->text << endl;
      bot.getApi().sendMessage(message->chat->id, ss.str(), false, 0, nullptr,
                               "Markdown");
    } else {
      printf("i am here\n");
      bot.getApi().sendMessage(message->chat->id, "Что-то не то...");
    }
  });

  signal(SIGINT, [](int s) {
    printf("SIGINT got\n");
    exit(0);
  });

  try {
    printf("Bot username: %s\n", bot.getApi().getMe()->username.c_str());
    bot.getApi().deleteWebhook();

    TgLongPoll longPoll(bot);
    while (true) {
      printf(".");
      longPoll.start();
    }
  } catch (exception& e) {
    printf("error: %s\n", e.what());
  }

  return 0;
}

void create_keyboard(const vector<string>& buttonStrings,
                     ReplyKeyboardMarkup::Ptr& kb) {
  for (size_t i = 0; i < buttonStrings.size(); ++i) {
    vector<KeyboardButton::Ptr> row;
    KeyboardButton::Ptr button(new KeyboardButton);
    acc.add(buttonStrings[i]);
    button->text = buttonStrings[i];
    button->requestContact = false;
    button->requestLocation = false;
    row.push_back(button);
    kb->keyboard.push_back(row);
  }
}

void create_keyboard(const vector<vector<string>>& buttonLayout,
                     ReplyKeyboardMarkup::Ptr& kb) {
  for (size_t i = 0; i < buttonLayout.size(); ++i) {
    vector<KeyboardButton::Ptr> row;
    for (size_t j = 0; j < buttonLayout[i].size(); ++j) {
      KeyboardButton::Ptr button(new KeyboardButton);
      acc.add(buttonLayout[i][j]);
      button->text = buttonLayout[i][j];
      button->requestContact = false;
      button->requestLocation = false;
      row.push_back(button);
    }
    kb->keyboard.push_back(row);
  }
}
