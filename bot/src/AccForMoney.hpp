#include <fstream>
#include <map>
#include <sstream>
#include <string>

#define PATH "data.csv"

using namespace std;

const vector<string> profit_categories = {
    "З.П.",
    "перевод",
    "что-то ещё..",
};
const vector<vector<string>> wastage_categories = {
    {"Еда", "Дорога", "Одежда"},
    {"Здоровье", "Баня", "Досуг"},
    {"Путешествие", "Подарок", "Копилка"},
    {"Учёба", "Подписка"}};

class AccForMoney {
 public:
  AccForMoney() : categoryes(){};
  AccForMoney(const string& filePath) : categoryes() { read_data(filePath); };
  void add(const string& new_categ) {
    if (categoryes.find(new_categ) == categoryes.end())
      categoryes[new_categ] = 0;
  };
  void add(const string& new_categ, int amount) {
    categoryes[new_categ] = amount;
  };
  void add(int amount) {
    if (wait_input) {
      categoryes[last_query] += amount;
      wait_input = false;
    }
  };
  bool in(const string item) {
    if (categoryes.find(item) != categoryes.end()) {
      wait_input = true;
      last_query = item;
      return true;
    }
    return false;
  };
  string str() {
    balancing();
    stringstream ss;
    ss << "Баланс " << profit << endl;
    ss << "Траты  " << spend << endl;
    for (const auto& categ : categoryes)
      if (!is_profit(categ.first))
        ss << setw(25) << setfill(' ') << categ.first << " = "
           << setprecision(2) << std::fixed
           << (100. * categ.second) / (double)spend << "%" << endl;
    return ss.str();
  }
  void save() {
    stringstream ss;
    for (const auto& categ : categoryes)
      ss << categ.first << "," << categ.second << endl;
    write(ss.str(), PATH);
  };

  bool wait_input{};
  string last_query;

 private:
  map<string, int> categoryes;
  int profit{};
  int spend{};
  void balancing() {
    profit = 0;
    spend = 0;
    for (auto it : categoryes)
      (is_profit(it.first)) ? (profit += it.second) : (spend += it.second);
    profit -= spend;
  };
  bool is_profit(string item) {
    return (find(profit_categories.begin(), profit_categories.end(), item) !=
            profit_categories.end());
  }
  string read(const string& filePath) {
    ifstream in(filePath, ios::in | ios::binary);
    in.exceptions(ifstream::failbit | ifstream::badbit);
    ostringstream contents;
    contents << in.rdbuf();
    in.close();
    return contents.str();
  }
  void write(const string& content, const string& filePath) {
    ofstream out(filePath, ios::out | ios::binary);
    out.exceptions(ofstream::failbit | ofstream::badbit);
    out << content;
    out.close();
  }
  void read_data(const string& file) {
    string tmp, data = read(file);
    stringstream ss(data);
    while (getline(ss, tmp, '\n')) {
      int m = tmp.find(",");
      add(tmp.substr(0, m), atoi((tmp.substr(m + 1, tmp.find("\n"))).data()));
    }
  }
};
